/**
 * @overview Wolfram Alpha integration for Slack
 */

const express = require('express');
const bodyParser = require('body-parser');
const slack = require('./module/slack');
const wolfram = require('./module/wolfram');

let app = express();

if (!process.env.PORT) {
  console.log('PORT ei määritelty.');
  process.exit();
}

/* Parse Slack's request body */
app.use(bodyParser.urlencoded({
  extended: true
}));

/* Handle Slack's webhooks */
app.post('/', (request, response) => {  
  let question = request.body.text;
  let responseUrl = request.body.response_url;
  
  if (!(question && responseUrl)) {
    response.send("Virheellinen pyyntö. Yritä uudelleen!");
  } else {
    let slackResponse = new slack.SlackResponse(responseUrl);
    
    wolfram.ask(question).then(answers => {
      slackResponse.send(null, answers);
    }).catch(err => {
      slackResponse.send(`Virhe: \`${err.message}\``);
    });
    
    response.end();
    slackResponse.send(`Hmmm... \`${question}\``);
  }
});

/* Start the server */
app.listen(parseInt(process.env.PORT));
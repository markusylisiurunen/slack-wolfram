/**
 * @overview Module for responding to Slack's webhooks.
 */

const axios = require('axios').create({
  headers: {'Content-Type': 'application/json'}
});


/**
 * Parse answers from environment variables.
 * @return {array} Array of answers.
 */
function parseAnswers() {
  let answers = [];
  let i = 1;
  
  while (process.env[`ANSWER_${i}`]) {
    answers.push(process.env[`ANSWER_${i}`]);
    i++;
  }
  
  return answers;
}


/**
 * Return a random answer to send along answers.
 * @return {string} A random answer.
 */
function getRandomAnswer() {
  let parsed = parseAnswers();
  let defaultAnswer = "Nonii..";
  let answers = parsed.length > 0 ? parsed : [defaultAnswer];
  
  return answers[Math.floor(Math.random() * answers.length)];
}


class SlackResponse {
  /**
   * Constructor for the SlackResponse.
   * @param {string} responseUrl The url to respond to.
   */
  constructor(responseUrl) {
    this._url = responseUrl;
  }
  
  /**
   * Send a response to Slack.
   * @param {string|null} text Text for the message.
   * @param {array} [attachments] Optional attachments.
   * @return {Promise} Promise for the request.
   */
  send(text, attachments = []) {
    let message = {
      response_type: 'in_channel',
      text: text || getRandomAnswer(),
      attachments: attachments
    };
    
    if (process.env.LOG_OUTPUT == 'true') {
      console.log(`Sent: ${JSON.stringify(message)}`);
      
      if (process.env.SLACK_HOOK) {
        return axios.post(process.env.SLACK_HOOK, message);
      } else {
        return Promise.resolve(true);
      }      
    } else {
      return axios.post(this._url, message);
    }
  }
};

exports.SlackResponse = SlackResponse;
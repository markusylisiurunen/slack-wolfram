/**
 * @overview Module for Wolfram Alpha's API
 */

const axios = require('axios');

const API_KEY = process.env.WOLFRAM_API_KEY;
const WOLFRAM_URL = 'https://api.wolframalpha.com/v1/query';

if (!API_KEY) {
  console.log('WOLFRAM_API_KEY ei määritelty.');
  process.exit();
}


/**
 * Parse the response from Wolfram Alpha's API.
 * @param  {string} response The raw response.
 * @return {array} Array of answers.
 */
function parseResponse(response) {
  let answers = [];

  if (response.success && response.pods.length > 0) {
    response.pods.forEach(pod => {
      let subpod = pod.subpods[0];      
      
      answers.push({
        title: pod.title,
        color: '#6b8698',
        image_url: subpod.img.src,
        fallback: subpod.img.alt
      });
    });
  } else {
    answers.push({
      title: 'Tapahtui virhe. Yritä uudelleen!',
      color: '#be5252'
    });
  }
  
  return answers;
}


/**
 * Ask a question from Wolfram Alpha.
 * @param  {string} question The question to ask.
 * @return {Promise} Promise for the answer.
 */
exports.ask = (question = '') => {
  return axios.get(WOLFRAM_URL, {
    params: {
      appid: API_KEY,
      output: 'json',
      input: question
    }
  }).then(response => {    
    return parseResponse(response.data.queryresult);
  });
};

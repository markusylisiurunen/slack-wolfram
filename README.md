# Wolfram Alpha integration for Slack

Ask any questions from Wolfram Alpha right from your Slack team.

## Usage

1. Clone this project and run `npm run build`.
2. Run the built Docker image somewhere where Slack can access it.
  - Look at `.env` to see what environment variables must be set.
3. Add slash command to your Slack team which points to your container.

## Testing

1. Clone this project and add your environment variables to `.env`.
2. Run `npm run build && npm run up`.
2. Use something like Postman to send POST requests to `localhost:8080`.
  - Requests must have `text` and `response_url` parameters in the body.
